Copyright (C) 2014 OPAP-JP contributors.
See authors.txt for full list. (すべての貢献者の一覧はauthors.txtをご覧ください。)

Licensed under the following license(s):
(以下の著作物利用許諾により利用が許諾されています:)

* CC-BY 2.1 JP
  http://creativecommons.org/licenses/by/2.1/jp/ 
  (shortened URL: http://opap.jp/cc/by/2.1/jp/)

* CC-BY 4.0
  http://creativecommons.org/licenses/by/4.0/ 
  (shortened URL: http://opap.jp/cc/by/4.0/)

* OPAP Upgrade License 1.0 Japan
  http://opap.jp/wiki/Licenses/OPAP-UP/1.0/jp 

  
Notes :  (補足事項:)
* For the avoidance of doubt, All text in authors.txt is a part of this credit.    
   (誤解を避けるために詳述すると、authors.txt内の全文はクレジットの一部です。)

